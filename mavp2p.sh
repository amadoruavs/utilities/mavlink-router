#!/bin/bash
# Starts mavp2p directly from the binary. Good for quick and dirty deployment;
# doesn't need to pull the docker image to run.
#
# While this configuration can and will work in production, it's likely
# better to use the Docker image instead, as the Docker image provides a
# more stable, guaranteed-to-work environment.
#
# Usage:
# ./mavp2p [server]
# Server can be:
# - A UDP MavLink stream (udps:<url>:14550, such as in sim)
# - A serial stream (serial:/dev/ttyUSB0:57600)

if [ "$1" = "" ]; then
    echo "You must specify a MavLink source (serial:/dev/ttyUSB0 or udps:<url>:14550)"
    exit 1
fi

./mavp2p $1 udpc:0.0.0.0:14551 udpc:0.0.0.0:14552 udpc:0.0.0.0:14553 udpc:0.0.0.0:14554 udpc:0.0.0.0:14555
